# Swagger

## Swagger简介

Vue + SpringBoot

后端时代：前端只用管理静态页面，html->后端。模板引擎 JSP==》后端是主力。



前后端分离时代：

* 后端：后端控制层，服务层，数据访问层【后端团队】
* 前端：前端控制层，视图层【前端团队】
  * 伪造后端数据json，已经存在了，，不需要后端，前端工程依然能够跑起来。
* 前后端如何交互？ ==》API
* 前后端相对独立，松耦合
* 前后端甚至可以部署在不同的服务器上



产生一个问题：

* 前后端集成联调，前端人员和后端人员无法做到，及时协商，尽早解决，最后导致问题集中爆发。

解决方案：

* 首先制定一个schema[计划的提纲]，实时更新最新API，降低集成的风险。
* 早些年：制定word计划文档；
* 前后端分离：
  * 前端测试后端接口：postman
  * 后端提供接口，需要实时更新最新的消息以及改动。

## Swagger

* 号称世界上最流行的API框架
* RESTful API 文档在线自动生成工具=>==API文档与API定义同步更新==
* 直接运行，可以在线直接测试API接口
* 支持多种语言：（Java，Php）

官网：https://swagger.io/



在项目中使用Swagger需要Springfox

* swagger2
* UI



## SpringBoot集成Swagger

![image-20221108200729578](swagger.assets/image-20221108200729578.png)

```xml
        <!-- https://mvnrepository.com/artifact/io.springfox/springfox-swagger2 -->
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger2</artifactId>
            <version>2.9.2</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/io.springfox/springfox-swagger-ui -->
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger-ui</artifactId>
            <version>2.9.2</version>
        </dependency>
```



1. 新建一个SpringBootWeb项目=web项目

2. 导入相关依赖（上面的俩）

3. 编写一个Hello工程

   ![image-20221108202230771](swagger.assets/image-20221108202230771.png)

4. 测试一下http://localhost:8080/swagger-ui.html

![image-20221108211612687](swagger.assets/image-20221108211612687.png)

## 配置Swagger

Swagger的bean实例Docket；

```java
package com.yibu.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.print.Doc;
import java.util.ArrayList;

@Configuration
@EnableSwagger2//开启Swagger2
public class SwaggerConfig {

    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo());
    }
    Contact DEFAULT_CONTACT = new Contact("依布","http://www.baidu.com","1563173710@qq.com");

    //配置Swagger信息=apiInfo
    private ApiInfo apiInfo(){
        return new ApiInfo(
                "依布的SwaggerAPI文档" ,
                "这个作者很牛逼",
                "1.0",
                "http://www.baidu.com",
                DEFAULT_CONTACT,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList());
    }
}
```

然后重启一下，效果就出来了。

![image-20221108220908026](swagger.assets/image-20221108220908026.png)

## Swagger配置扫描接口

Docket.select

```java
    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                //RequestHandlerSelectors 配置要扫描接口的方式
                //basePackage:指定要扫描的包
                //any():扫描全部
                //none():不扫描
                //withClassAnnotation(:扫描类上的注解 参数是注解的反射对象 RestController.class
                //withMethodAnnotation(:扫描方法上的注解  GetMapping.class
                //一般就是扫描包，下面的这个
                .apis(RequestHandlerSelectors.basePackage("com.yibu.controller"))
                //paths() 过了什么路径
                //.paths(PathSelectors.ant("/com/**/**")) 我这个写的有点问题
                .build();
    }
```

enable是否启动Swagger,如果为False，则Swagger不能在浏览器中访问。

```java
    //配置了Swagger的Docket的bean实例
    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .enable(false)//enable是否启动Swagger,如果为False，则Swagger不能在浏览器中访问。
                .select()
                //RequestHandlerSelectors 配置要扫描接口的方式
                //basePackage:指定要扫描的包
                //any():扫描全部
                //none():不扫描
                //withClassAnnotation(:扫描类上的注解 参数是注解的反射对象 RestController.class
                //withMethodAnnotation(:扫描方法上的注解  GetMapping.class
                //一般就是扫描包，下面的这个
                .apis(RequestHandlerSelectors.basePackage("com.yibu.controller"))
                //paths() 过了什么路径
                //.paths(PathSelectors.ant("/com/**/**")) 我这个写的有点问题
                .build();
    }
```

![image-20221108222226017](swagger.assets/image-20221108222226017.png)



## 配置API文档的分组

![image-20221108223940140](swagger.assets/image-20221108223940140.png)

![image-20221108223959682](swagger.assets/image-20221108223959682.png)

可以配置多个Docket类，然后放到Spring容器中，设置不同的分组。各自扫各自的方法，生成文档。然后组名不同，可以在有上角进行切换。

![image-20221108224223564](swagger.assets/image-20221108224223564.png)



## 实体类配置

只要是接口返回的实体类，就会被swagger给扫描到。

```java
@ApiModel("用户实体类")
public class User {
    @ApiModelProperty("用户名")
    private String username;
    @ApiModelProperty("密码")
    private String password;
```

```java
@RestController
public class HelloController {
    @GetMapping(value = "/hello")
    public String hello(){
        return "hello world";
    }
    //只要我们的接口中，返回值中存在实体类，它就会被扫描到Swagger中
    @PostMapping(value = "/user")
    public User user(){
        return new User("kebi","1234");
    }
}
```

![image-20221108225549108](swagger.assets/image-20221108225549108.png)



方法也可以加注释

```java
@RestController
public class HelloController {

    @ApiOperation("Hello依布的控制类的HelloWorld")
    @GetMapping(value = "/hello")
    public String hello(){
        return "hello world";
    }
```

![image-20221108225851220](swagger.assets/image-20221108225851220.png)



也可以给参数加

```java
@PostMapping(value = "/user")
public User user(@ApiParam("用户名") String username){
    return new User(username,"1234");
}
```

![image-20221108230030459](swagger.assets/image-20221108230030459.png)



## Swagger接口测试

```java
    @PostMapping(value = "/user")
    public User user(@ApiParam("用户名") @RequestParam("username") String username){
        System.out.println("username:"+username);
        return new User(username,"1234");
    }
```

![image-20221108232136296](swagger.assets/image-20221108232136296.png)

![image-20221108232153524](swagger.assets/image-20221108232153524.png)





总结：

1. 我们可以通过Swagger给一些比较难理解的属性或者接口，增加注释信息
2. 接口文档实时更新
3. 可以在线测试

Swagger是一个优秀的工具，几乎所有的大公司都有使用。

【注意点】在正式发布的时候，关闭Swagger！！出于安全考虑，还能节省内存。







