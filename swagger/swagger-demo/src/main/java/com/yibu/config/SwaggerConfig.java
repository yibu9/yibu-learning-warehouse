package com.yibu.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.function.Predicate;

@Configuration
@EnableSwagger2//开启Swagger2
public class SwaggerConfig {

    //配置了Swagger的Docket的bean实例
    @Bean
    public Docket docket(Environment environment){

        //设置要显示的Swagger环境
        Profiles profiles = Profiles.of("dev","test");
        //通过environment.acceptsProfiles判断是否处在自己设定的环境中
        boolean flag = environment.acceptsProfiles(profiles);

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("1组")
                .enable(flag)//enable是否启动Swagger,如果为False，则Swagger不能在浏览器中访问。
                .select()
                //RequestHandlerSelectors 配置要扫描接口的方式
                //basePackage:指定要扫描的包
                //any():扫描全部
                //none():不扫描
                //withClassAnnotation(:扫描类上的注解 参数是注解的反射对象 RestController.class
                //withMethodAnnotation(:扫描方法上的注解  GetMapping.class
                //一般就是扫描包，下面的这个
                .apis(RequestHandlerSelectors.basePackage("com.yibu"))
                //paths() 过了什么路径
                //.paths(PathSelectors.ant("/com/**/**")) 我这个写的有点问题
                .build();
    }
    Contact DEFAULT_CONTACT = new Contact("依布","http://www.baidu.com","1563173710@qq.com");

    //配置Swagger信息=apiInfo
    private ApiInfo apiInfo(){
        return new ApiInfo(
                "依布的SwaggerAPI文档" ,
                "这个作者很牛逼",
                "1.0",
                "http://www.baidu.com",
                DEFAULT_CONTACT,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList());
    }

}
