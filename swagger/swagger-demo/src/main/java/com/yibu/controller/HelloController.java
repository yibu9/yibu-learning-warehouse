package com.yibu.controller;

import com.yibu.pojo.User;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;


@RestController
public class HelloController {

    @ApiOperation("Hello依布的控制类的HelloWorld")
    @GetMapping(value = "/hello")
    public String hello(){
        return "hello world";
    }

    //只要我们的接口中，返回值中存在实体类，它就会被扫描到Swagger中
    @PostMapping(value = "/user")
    public User user(@ApiParam("用户名") @RequestParam("username") String username){
        System.out.println("username:"+username);
        return new User(username,"1234");
    }
}
